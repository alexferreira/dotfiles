#!/bin/sh

if [ ! -d "$HOME/.dotfiles" ]; then
    echo "Setup dotfiles for the first time"
    git clone --depth=1 https://gitlab.com/alexferreira/dotfiles.git "$HOME/.dotfiles"
    cd "$HOME/.dotfiles"
    git submodule init
    git submodule update
    [ "$1" = "ask" ] && export ASK="true"
    rake install
else
    echo "dotfiles has been installed"
fi

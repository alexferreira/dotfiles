require 'rake'
require 'fileutils'

desc "Hook our dotfiles into system-standard positions."
task :install => [] do
  puts
  puts "============================================================"
  puts "Setup."
  puts "============================================================"
  puts

  install_fonts

  install_homebrew if RUBY_PLATFORM.downcase.include?("darwin")

  # this has all the runcoms from this directory.
  install_files(Dir.glob('git/*')) if want_to_install?('git configs (color, aliases)')
  install_files(Dir.glob('tmux/*')) if want_to_install?('tmux config')
  install_files(Dir.glob('irb/*')) if want_to_install?('irb/pry configs (more colorful)')
  install_files(Dir.glob('ruby/*')) if want_to_install?('rubygems config (faster/no docs)')

  msg("installed")
end

task :osx => [] do
  puts "============================================================"
  puts "Optimize osx."
  puts "============================================================"
  run %{ sh "$HOME/.dotfiles/bin/osx" }
  puts "============================================================"
end

desc 'Updates the installation'
task :update do
  Rake::Task["install"].execute
end

task :default => 'install'

private
def run(cmd)
  puts "[Running] #{cmd}"
  `#{cmd}` unless ENV['DEBUG']
end

def install_fonts
  puts "============================================================"
  puts "Installing patched fonts."
  puts "============================================================"
  run %{ cp -f $HOME/.dotfiles/fonts/* $HOME/Library/Fonts } if RUBY_PLATFORM.downcase.include?("darwin")
  run %{ mkdir -p ~/.fonts && cp ~/.dotfiles/fonts/* ~/.fonts && fc-cache -vf ~/.fonts } if RUBY_PLATFORM.downcase.include?("linux")
  puts
end

def install_homebrew
  run %{which brew}
  unless $?.success?
    puts "============================================================"
    puts "Installing Homebrew, the OSX package manager...If it's"
    puts "already installed, this will do nothing."
    puts "============================================================"
    run %{ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"}    
  end

  puts
  puts
  puts "============================================================"
  puts "Updating Homebrew."
  puts "============================================================"
  run %{brew update}
  puts
  puts
  puts "============================================================"
  puts "Installing Homebrew packages...There may be some warnings."
  puts "============================================================"
  run %{brew install ctags hub tmux reattach-to-user-namespace the_silver_searcher}
  puts
  puts
end

def want_to_install? (section)
  if ENV["ASK"]=="true"
    puts "Would you like to install configuration files for: #{section}? [y]es, [n]o"
    STDIN.gets.chomp == 'y'
  else
    true
  end
end

def install_files(files, method = :symlink)
  files.each do |f|
    file = f.split('/').last
    source = "#{ENV["PWD"]}/#{f}"
    target = "#{ENV["HOME"]}/.#{file}"

    puts "======================#{file}=============================="
    puts "Source: #{source}"
    puts "Target: #{target}"

    if File.exists?(target) && (!File.symlink?(target) || (File.symlink?(target) && File.readlink(target) != source))
      puts "[Overwriting] #{target}...leaving original at #{target}.backup..."
      run %{ mv "$HOME/.#{file}" "$HOME/.#{file}.backup" }
    end

    if method == :symlink
      run %{ ln -nfs "#{source}" "#{target}" }
    else
      run %{ cp -f "#{source}" "#{target}" }
    end

    puts "============================================================"
    puts
  end
end

def msg(action)
  puts ""
  puts "dotfiles has been #{action}. Please restart your terminal."
end
